#!/bin/sh

DIR=$(dirname $(realpath $0))
TEMP="$(realpath $DIR/temp)"
OUTPUT="$(realpath $DIR/output)"

[ -d $OUTPUT ] && echo "delete $DIR/output before run this script" && exit 0
[ -d $TEMP ] && echo "delete $DIR/temp before rut this script" && exit 0
mkdir $TEMP $OUTPUT
echo "### checking SHA1 for zip"
echo "d39658dd88ef22e7dc1c7a6ee529a59bbf80a798  $DIR/dict_revised_2015/dict_revised_2015_20180409.zip" | sha1sum --quiet -c || exit 0
#echo "### checking SHA512 for zip"
#echo "e771dd24d398dcb933cc4e8f9b31df1937de6199f30a11219fb34dda38a4ddd63a893f08d629f01be1f8ed5be66ca58d699ec73e942dc4a95a21cc7e7dbcd1d7 $DIR/dict_revised_2015/dict_revised_2015_20180409.zip" | sha512sum --quiet -c || exit 0

echo "### extracting xls from zip"
unzip -q $DIR/dict_revised_2015/dict_revised_2015_20180409.zip dict_revised_2015_20180409_1.xls dict_revised_2015_20180409_2.xls dict_revised_2015_20180409_3.xls -d $TEMP
#7z e $DIR/dict_revised_2015/dict_revised_2015_20180409.zip dict_revised_2015_20180409_1.xls dict_revised_2015_20180409_2.xls dict_revised_2015_20180409_3.xls -o$TEMP >/dev/null

echo "### compiling xl2text_dict_revised.c"
gcc $DIR/xl2text_dict_revised.c -lfreexl -o $TEMP/xl2text

echo "### converting xls to txt"
echo "教育部重編國語辭典修訂本 2015\nhttps://resources.publicense.moe.edu.tw/dict_reviseddict_download.html" > $TEMP/0.txt
$TEMP/xl2text $TEMP/dict_revised_2015_20180409_1.xls | sed 's/^\[/ \[/; /^ /! s/^/     /; s/^ headword//' > $TEMP/1.txt
$TEMP/xl2text $TEMP/dict_revised_2015_20180409_2.xls | sed 's/^\[/ \[/; /^ /! s/^/     /; s/^ headword//' > $TEMP/2.txt
$TEMP/xl2text $TEMP/dict_revised_2015_20180409_3.xls | sed 's/^\[/ \[/; /^ /! s/^/     /; s/^ headword//' > $TEMP/3.txt
cat $TEMP/0.txt $TEMP/1.txt $TEMP/3.txt > $TEMP/dict_revised_2015.txt

cd $OUTPUT
echo "### dictfmt"
dictfmt -f --utf8 -s "教育部重編國語辭典修訂本 2015 20180409" dict_revised_2015 < $TEMP/dict_revised_2015.txt
echo "### dictzip"
dictzip dict_revised_2015.dict

## delete the temporary folder
rm -rf $TEMP && echo "### the temporary folder was removed"
