/* 
/ this file is modified from xl2sql.c (FreeXL Sample code, Author: Sandro Furieri a.furieri@lqt.it)
/
/ ------------------------------------------------------------------------------
/ 
/ Version: MPL 1.1/GPL 2.0/LGPL 2.1
/ 
/ The contents of this file are subject to the Mozilla Public License Version
/ 1.1 (the "License"); you may not use this file except in compliance with
/ the License. You may obtain a copy of the License at
/ http://www.mozilla.org/MPL/
/ 
/ Software distributed under the License is distributed on an "AS IS" basis,
/ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
/ for the specific language governing rights and limitations under the
/ License.
/
/ The Original Code is the FreeXL library
/
/ The Initial Developer of the Original Code is Alessandro Furieri
/ 
/ Portions created by the Initial Developer are Copyright (C) 2011
/ the Initial Developer. All Rights Reserved.
/ 
/ Contributor(s):
/ Brad Hards
/ 
/ Alternatively, the contents of this file may be used under the terms of
/ either the GNU General Public License Version 2 or later (the "GPL"), or
/ the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
/ in which case the provisions of the GPL or the LGPL are applicable instead
/ of those above. If you wish to allow use of your version of this file only
/ under the terms of either the GPL or the LGPL, and not to allow others to
/ use your version of this file under the terms of the MPL, indicate your
/ decision by deleting the provisions above and replace them with the notice
/ and other provisions required by the GPL or the LGPL. If you do not delete
/ the provisions above, a recipient may use your version of this file under
/ the terms of any one of the MPL, the GPL or the LGPL.
/ 
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "freexl.h"


int
main (int argc, char *argv[])
{
    unsigned int worksheet_index;
    const void *handle;
    int ret;
    unsigned int max_worksheet;
    unsigned int rows;
    unsigned short columns;
    unsigned int row;
    unsigned short col;

/* opening the .XLS file [Workbook] */
    ret = freexl_open (argv[1], &handle);
    if (ret != FREEXL_OK)
      {
	  fprintf (stderr, "OPEN ERROR: %d\n", ret);
	  return -1;
      }

/* querying BIFF Worksheet entries */
    ret = freexl_get_info (handle, FREEXL_BIFF_SHEET_COUNT, &max_worksheet);
    if (ret != FREEXL_OK)
      {
	  fprintf (stderr, "GET-INFO [FREEXL_BIFF_SHEET_COUNT] Error: %d\n",
		   ret);
	  goto stop;
      }


    for (worksheet_index = 0; worksheet_index < max_worksheet;
	 worksheet_index++)
      {
	  const char *utf8_worsheet_name;
	  ret =
	      freexl_get_worksheet_name (handle, worksheet_index,
					 &utf8_worsheet_name);
	  if (ret != FREEXL_OK)
	    {
		fprintf (stderr, "GET-WORKSHEET-NAME Error: %d\n", ret);
		goto stop;
	    }
	  /* selecting the active Worksheet */
	  ret = freexl_select_active_worksheet (handle, worksheet_index);
	  if (ret != FREEXL_OK)
	    {
		fprintf (stderr, "SELECT-ACTIVE_WORKSHEET Error: %d\n", ret);
		goto stop;
	    }
	  /* dimensions */
	  ret = freexl_worksheet_dimensions (handle, &rows, &columns);
	  if (ret != FREEXL_OK)
	    {
		fprintf (stderr, "WORKSHEET-DIMENSIONS Error: %d\n", ret);
		goto stop;
	    }

	  //printf ("--\n-- populating the same table\n--\n");
	  for (row = 0; row < rows; row++)
	    {
		/* INSERT INTO statements */
		FreeXL_CellValue cell;

		freexl_get_cell_value (handle, row, 2, &cell);
		printf (" headword%s\n", cell.value.text_value);
		for (col = 0; col < columns; col++)
		  {
		      freexl_get_cell_value (handle, row, col, &cell);
		      switch (cell.type)
			{
			case FREEXL_CELL_INT:
			    printf ("[int], %d", cell.value.int_value);
			    break;
			case FREEXL_CELL_DOUBLE:
			    printf ("[double], %1.12f", cell.value.double_value);
			    break;
			case FREEXL_CELL_TEXT:
			case FREEXL_CELL_SST_TEXT:
			    switch (col)
				{
				case 0:
				printf ("     字詞屬性  %s\n", cell.value.text_value);
				break;
				case 1:
				printf ("       字詞號  %s\n", cell.value.text_value);
				break;
				case 2:
				printf ("       字詞名  %s\n", cell.value.text_value);
				break;
				case 3:
				printf ("       部首字  %s\n", cell.value.text_value);
				break;
				case 4:
				printf (" 部首外筆劃數  %s\n", cell.value.text_value);
				break;
				case 5:
				printf ("     總筆劃數  %s\n", cell.value.text_value);
				break;
				case 6:
				printf ("     注音一式  %s\n", cell.value.text_value);
				break;
				case 7:
				printf ("     漢語拼音  %s\n", cell.value.text_value);
				break;
				case 8:
				printf ("       相似詞  %s\n", cell.value.text_value);
				break;
				case 9:
				printf ("      相似詞1  %s\n", cell.value.text_value);
				break;
				case 10:
				printf ("      相似詞2  %s\n", cell.value.text_value);
				break;
				case 11:
				printf ("      相似詞3  %s\n", cell.value.text_value);
				break;
				case 12:
				printf ("      相似詞4  %s\n", cell.value.text_value);
				break;
				case 13:
				printf ("       相反詞  %s\n", cell.value.text_value);
				break;
				case 14:
				printf ("         釋義\n\n%s\n\n", cell.value.text_value);
				break;
				case 15:
				printf ("         編按  %s\n", cell.value.text_value);
				break;
				case 16:
				printf (" 多音參見訊息  %s\n", cell.value.text_value);
				break;
				case 17:
				printf ("       異體字  %s\n", cell.value.text_value);
				break;
				}
			    break;
			case FREEXL_CELL_DATE:
			case FREEXL_CELL_DATETIME:
			case FREEXL_CELL_TIME:
			    printf ("[time], '%s'", cell.value.text_value);
			    break;
			case FREEXL_CELL_NULL:
			default:
			    //printf ("[NULL]\n");
			    break;
			};
		  }
	    }
      }

  stop:
/* closing the .XLS file [Workbook] */
    ret = freexl_close (handle);
    if (ret != FREEXL_OK)
      {
	  fprintf (stderr, "CLOSE ERROR: %d\n", ret);
	  return -1;
      }

    return 0;
}
