### Generate DICT database of 《重編國語辭典修訂本》 from XLS
Warning: It just works, there are still some formatting issues of the generated DICT database.

#### Dependencies
```
unzip
gcc
freexl
freexl-devel
sed
dictd
```

#### Generate the DICT database
Download [dict\_revised\_2015\_20180409.zip](https://resources.publicense.moe.edu.tw/download/dict_revised_2015_20180409.zip) from [教育部國語辭典公眾授權網](https://resources.publicense.moe.edu.tw/dict_reviseddict_download.html) and save it to `dict_revised_2015/`
```
$ wget https://resources.publicense.moe.edu.tw/download/dict_revised_2015_20180409.zip -O dict_revised_2015/dict_revised_2015_20180409.zip
```
Run this script
```
$ ./generate_dict_from_xls.sh
```
The DICT database will be generated under folder "output"
```
$ ls output
dict_revised_2015.dict.dz  dict_revised_2015.index
```

#### Use the DICT database in dictd
Copy the DICT database to `/usr/share/dictd/dict_revised_2015`
```
# mkdir -p /usr/share/dictd/dict_revised_2015
# cp output/dict_revised_2015.dict.dz dict_revised_2015.index /usr/share/dictd/dict_revised_2015
```
Add these lines to `/etc/dict/dictd.conf`
```
database dict_revised_2015 {
  data "/usr/share/dictd/dict_revised_2015/dict_revised_2015.dict.dz"
  index "/usr/share/dictd/dict_revised_2015/dict_revised_2015.index"
}
```
Start the service of dictd, then you should be able to query words in dictd clients suck as `dict`, GNOME Dictionary and GNU Dico web, for example: `dict -d dict_revised_2015 之`

#### Note
xl2text\_dict\_revised.c is modified from xl2sql.c (the sample code from [FreeXL](https://www.gaia-gis.it/fossil/freexl)), license is included in the file.
dict\_revised\_2015\_20180409.zip is licensed under [CC BY-ND 3.0 Taiwan](http://creativecommons.org/licenses/by-nd/3.0/tw/legalcode) by Ministry of Education Republic of China (Taiwan), so you may not share the DICT DATABASE file you generated.
